package com.mypdc.chatapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity


class chat : AppCompatActivity() {

    var mEmail: EditText? = null
    var mPwd: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        mEmail = findViewById(R.id.email)
        mPwd = findViewById(R.id.password)

        var btn: Button = findViewById(R.id.submit_btn)
        btn.setOnClickListener {
            if(validate())
            {
                var result:String?= null
                if(mEmail?.text.toString().equals("hamza@vizrex.com")&& mPwd?.text.toString().equals("123456"))
                {
                    result= "validate successful"
                }else{

                    result="fail"
                }

                val returnIntent = Intent()
                returnIntent.putExtra("result", result)
                setResult(101, returnIntent)
                finish()
            }


        }


    }

    private fun validate():Boolean {
        var flag:Boolean= true
        if (mEmail?.text.toString().equals("")) {
            mEmail?.error = "Please enter email address!"
            flag= false
        }

        if (mPwd?.text.toString().equals("")) {
            mPwd?.error = "Please enter  password!"
            flag= false
        }

        return flag

    }
}
